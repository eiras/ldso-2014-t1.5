/**
 * Created by Miguel on 24-11-2014.
 */

$( document ).ready(function() {

    $painel_signup = $('#painel_signup');
    $painel_login = $('#painel_login');

    $("#signup").click(function(e) {
        $painel_login.toggleClass("collapse");
        $painel_signup.toggleClass("collapse");
        $("#signup").toggleClass("collapse");
        $("#login").toggleClass("collapse");

    });

    $("#login").click(function(e) {
        $painel_login.toggleClass("collapse");
        $painel_signup.toggleClass("collapse");
        $("#signup").toggleClass("collapse");
        $("#login").toggleClass("collapse");
    });
});
