/**
 * Created by Miguel on 16-10-2014.
 */

function initialize() {

    var myLatlng = new google.maps.LatLng(38.736946,-9.142685);

    var mapOptions = {
        center: myLatlng,
        zoom: 15
    };
    var map = new google.maps.Map(document.getElementById('map_canvas'),
        mapOptions);

    // Resize stuff...
    google.maps.event.addDomListener(window, "resize", function() {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });

    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title:"Basquetebol no Parque"
    });

}

$( document ).ready(function() {
    mapa = $('#map_canvas');
    mapa.height( mapa.parent().height() );
});


google.maps.event.addDomListener(window, 'load', initialize);

