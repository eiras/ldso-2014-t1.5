/**
 * Created by Miguel on 16-10-2014.
 */

function initialize() {

    var myLatlng = new google.maps.LatLng(38.736946,-9.142685);
    var myLatlng1 = new google.maps.LatLng(41.1665933,-8.6039077);
    var myLatlng2 = new google.maps.LatLng(41.1736479,-8.5986399);
    var myLatlng3 = new google.maps.LatLng(41.1819733,-8.6067803);
    var myLatlng4 = new google.maps.LatLng(41.1753072,-8.6019631);
    var myLatlng5 = new google.maps.LatLng(41.1756464,-8.6019845);
    var myLatlng6 = new google.maps.LatLng(41.1738859,-8.5969849);

    var mapOptions = {
        center: { lat: 38.736946, lng: -9.142685},
        zoom: 2
    };

    map = new google.maps.Map(document.getElementById('map_canvas'),
        mapOptions);

    marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title:"Basquetebol no Parque"
    });
    marker1 = new google.maps.Marker({
        position: myLatlng1,
        map: map,
        title:"Corrida na Praia"
    });
    marker2 = new google.maps.Marker({
        position: myLatlng2,
        map: map,
        title:"Futebol Amigável"
    });
    marker3 = new google.maps.Marker({
        position: myLatlng3,
        map: map,
        title:"Jogging Matinal"
    });
    marker4 = new google.maps.Marker({
        position: myLatlng4,
        map: map,
        title:"Ping-Pong"
    });
    marker5 = new google.maps.Marker({
        position: myLatlng5,
        map: map,
        title:"Ténis"
    });
    marker6 = new google.maps.Marker({
        position: myLatlng6,
        map: map,
        title:"Caminhada Solidária"
    });

    // Resize stuff...
    google.maps.event.addDomListener(window, "resize", function() {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });

}

$( document ).ready(function() {
    $('#map_canvas').height( $( "body" ).height() );

    $('#sugeridos').click(function(){

        if( $(this).hasClass("selected") ){

            $(this).removeClass("selected");
            marker3.setVisible(true);
            marker4.setVisible(true);
            marker5.setVisible(true);
            marker6.setVisible(true);
        }
        else {
            $(this).addClass("selected");
            marker3.setVisible(false);
            marker4.setVisible(false);
            marker5.setVisible(false);
            marker6.setVisible(false);
        }
    });

    $('#perto').click(function(){

        if( $(this).hasClass("selected") ){

            $(this).removeClass("selected");
            marker.setVisible(true);
        }
        else {
            $(this).addClass("selected");
            marker.setVisible(false);
        }
    });

    $('#amigos').click(function(){

        if( $(this).hasClass("selected") ){

            $(this).removeClass("selected");
            marker3.setVisible(true);
            marker4.setVisible(true);
        }
        else {
            $(this).addClass("selected");
            marker3.setVisible(false);
            marker4.setVisible(false);
        }
    });
});


google.maps.event.addDomListener(window, 'load', initialize);