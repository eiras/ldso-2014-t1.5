var mapLoaded = false;
var markers = [];

var adjust = function() 
{
	var fontSize = parseInt($(".leftArrow-div").width());
	if(fontSize>45)
	fontSize = 45;
	else if(fontSize<25)
	fontSize=25;

	fontSize = fontSize + "px";
//alert(fontSize);
	$(".leftArrow-div i").css('font-size', fontSize);	    

	var fontSize = parseInt($(".rightArrow-div").width());
	if(fontSize>45)
		fontSize = 45;
	else if(fontSize<25)
	 	fontSize=25;
	
	fontSize = fontSize + "px";
	//alert(fontSize);
	$(".rightArrow-div i").css('font-size', fontSize);

	var fontSize = parseInt($(".icone-div").width()-5 );

	$(".icone-div a").css('font-size', fontSize);

	var fontSize = parseInt($(".icone-div1").width()-5 );

		$(".icone-div1 a").css('font-size', fontSize);

};



function atualizaPag6()
{
	adjust();
	 
	var data = $('[name="data"]').val();
	if (data == "")
	{
		data ="Sem data definida";
	}
	
	var hora = $('[name="hora"]').val();
	if(hora =="")
		hora ="Sem hora definida";
	
	
	var nome = $('[name="nomeEvento"]').val();
	if(nome =="")
		nome = "Sem nome definido";
	
	var descricao = $('[name="descricaoEvento"]').val();
	if(descricao=="")
		 descricao ="Sem descricao";

	var desporto = $('[name="radioDesporto"]:checked').val();	
	
	var desportoClass = $('[name="radioDesporto"]:checked + a').attr('class');
	var limiteParticipantes = $('[name="limiteParticipantes"]:checked').val();
	var participantes = $('[name="participantes"]').val();
	
	var morada = $('[name="morada"]').val();
	if(morada=="")
		morada ="sem morada";
	//var coordenadas = markers[0].getPosition().toString();
	
	console.log("data : ",data,"   hora: ",hora,"   nome: ",nome,"   desporto: ",desporto,"   desportoClass: ",desportoClass,"   descricao: ",descricao,"   morada: ",morada);
	

	$('#desporto').html(desporto);
	$('#dataHora').html(data + " " + hora);
	if(limiteParticipantes === "sim")
	{
		$('#limite').html("Limite de <b>" + participantes + "</b> pessoas");
	}
	else if(limiteParticipantes === "nao")
	{
		$('#limite').html("Sem limite de participantes");
	}
	else 
	{
		$('#limite').html("não definido limite de participantes");
	}

	$('#evento_nome').html(nome);
	$('#evento_descricao').html(descricao);
	$('#desporto_icone').attr('class',desportoClass);
	$('#local_name').html(morada);

	if(!markers[0])
	{}
	else initializeMapWithLocation();


}


				function getActive()
				{
					var pag = 0;
					for (var i = 1; i < 7; i++) {
						var p = '#pag' + i;
						if($(p).hasClass('pagAtiva'))
						{
							pag = i;
							break;
						}
					};
					return pag;
				};

				function nextPage()
				{
					var pageId = getActive();
					console.log(pageId);
					if(pageId<6)
					{
						$('#pag' + pageId).fadeOut(500, function() 
						{	   
							$('#pag' + pageId).toggleClass('pagAtiva');
							pageId++;
							$('#pag' + pageId).show();
							$('#pag' + pageId).toggleClass('pagAtiva');

							if(pageId == 4 && !mapLoaded)
							{
								initialize();
								mapLoaded = true;
							}

							if(pageId == 6)
							{
								atualizaPag6();
							}
						});
					}
					else if(pageId==6)
					{
						
						$("#formEvent").submit();

					}

				};
				function previousPage()
				{
					var pageId = getActive();
					console.log(pageId);
					if(pageId>1)
					{
						$('#pag' + pageId).fadeOut(500, function() 
						{	   
							$('#pag' + pageId).toggleClass('pagAtiva');
				            pageId--;
							$('#pag' + pageId).show();
					        $('#pag' + pageId).toggleClass('pagAtiva');
					        });
					}
				};


				function showNumParticipantes()
				{
					$('#numParticipantes').fadeIn(500);
				};

				function hideNumParticipantes()
				{
					$('#numParticipantes').fadeOut(500);
				};

$(document).ready(adjust);
$(window).resize(adjust);


/* Code From: https://developers.google.com/maps/documentation/javascript/examples/places-searchbox */

// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initialize() {

  markers = [];
  var map = new google.maps.Map(document.getElementById('map-canvas'), {
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  var defaultBounds = new google.maps.LatLngBounds(
      new google.maps.LatLng(-33.8902, 151.1759),
      new google.maps.LatLng(-33.8474, 151.2631));
  map.fitBounds(defaultBounds);

  // Create the search box and link it to the UI element.
  var input = /** @type {HTMLInputElement} */(
      document.getElementById('pac-input'));
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  var searchBox = new google.maps.places.SearchBox(
    /** @type {HTMLInputElement} */(input));

  // Listen for the event fired when the user selects an item from the
  // pick list. Retrieve the matching places for that item.
  google.maps.event.addListener(searchBox, 'places_changed', function() {
    var places = searchBox.getPlaces();

    if (places.length == 0) {
      return;
    }
    for (var i = 0, marker; marker = markers[i]; i++) {
      marker.setMap(null);
    }

    // For each place, get the icon, place name, and location.
    markers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0, place; place = places[i]; i++) {
      var image = {
        url: place.icon,
        size: new google.maps.Size(71, 71),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(17, 34),
        scaledSize: new google.maps.Size(25, 25)
      };

      // Create a marker for each place.
      var marker = new google.maps.Marker({
        map: map,
        icon: image,
        title: place.name,
        position: place.geometry.location
      });

      markers.push(marker);

      bounds.extend(place.geometry.location);
    }

    map.fitBounds(bounds);
  });

  // Bias the SearchBox results towards places that are within the bounds of the
  // current map's viewport.
  google.maps.event.addListener(map, 'bounds_changed', function() {
    var bounds = map.getBounds();
    searchBox.setBounds(bounds);
  });


}

//google.maps.event.addDomListener(window, 'load', initialize);
function initializeMapWithLocation() {

 
  var pos = markers[0].position;


  var mapOptions = {
    zoom: 6,
    center: pos
  }
  var map = new google.maps.Map(document.getElementById("map_canvas1"), mapOptions);

  var marker = new google.maps.Marker({
    position: pos,
    map: map
    
});

}


function ValidationEvent()
{
	var data = $('[name="data"]').val();
	var hora = $('[name="hora"]').val();
	var nome = $('[name="nomeEvento"]').val();
	var descricao = $('[name="descricaoEvento"]').val();
	var desporto = $('[name="radioDesporto"]:checked').val();	
	var desportoClass = $('[name="radioDesporto"]:checked + a').attr('class');
	var limiteParticipantes = $('[name="limiteParticipantes"]:checked').val();
	var participantes = $('[name="participantes"]').val();
	var morada = $('[name="morada"]').val();
		
		
 	if(data =="" || hora == "" || nome == "" || descricao == "" || desporto =="" || morada =="" || limiteParticipantes =="")
     	{
			alert( "Faltam Preencher campos" );
			return false;
		}  
		else if (limiteParticipantes =="sim" && participantes =="")
		{
			alert( "Falta Preencher o limite de participantes" );
			return false;
		}
		else 
		{
		var coordenadas = markers[0].getPosition().toString();
										
	 		$('#formEvent').append("<input type='hidden' name='coordenadas' value='"+
	            coordenadas+"' />");

	 		return true;
    	}

}
