Feature: Sign up to the system
 
Scenario: Sign up
	
	Given I am on the "login" page
	Then I will click the "Sign Up" button
	Then I will fill the signup form with first name "John" last name "Doe" email "johndoe@gmail.com" birth "07/07/1980" password "rails"
	Then I will click the "Sign up" button
	When I go to the homepage
	Then I should see "John"