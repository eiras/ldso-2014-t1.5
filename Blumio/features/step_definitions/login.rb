Given(/^I am on the "(.*?)" page$/) do |page|
  visit page
end

Then(/^I will fill the login form with email "(.*?)" and password "(.*?)"$/) do |email, password|
	fill_in 'session_email', :with => email
	fill_in 'session_password_digest', :with => password
end

Then(/^I will click the "(.*?)" button$/) do |button|
	has_button?(button)
	click_button(button)
end

Then(/^I should see "(.*?)"$/) do |text|
	page.assert_text(text)
end

Then(/^I will click the "(.*?)" link$/) do |link|
	has_link?(link)
	click_link(link)
end

Then(/^I will fill the signup form with first name "(.*?)" last name "(.*?)" email "(.*?)" birth "(.*?)" password "(.*?)"$/) do |first, last, email, birth, password|
	fill_in 'person_first_name', :with => first
	fill_in 'person_last_name', :with => last
	fill_in 'signup_email', :with => email
	fill_in 'person_birthday', :with => birth
	fill_in 'person_password', :with => password
	fill_in 'person_password_confirmation', :with => password
end

When /^I go to the homepage$/ do
	visit root_path
end

