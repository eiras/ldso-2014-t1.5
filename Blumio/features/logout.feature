Feature: Log out from the system
 
Scenario: Log out
	
	Given I am on the "login" page
	Then I will fill the login form with email "teste1@blumio.com" and password "cenas"
	Then I will click the "Login" button
	Then I should see "O que queres fazer hoje?"
	Then I will click the "menu-toggle" link
	Then I will click the "Sair" link
	Then I should see "Log in"