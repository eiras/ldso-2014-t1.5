Rails.application.routes.draw do


  get 'notifications/index'

  ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config

  get 'home'    => 'static_pages#home'
  get 'search'  => 'events#searchAll'
  post 'search' => 'events#search'
  get 'searchList' => 'events#searchListAll'
  post 'searchList' => 'events#searchList'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  post 'events_people/:id' => 'events#join'
  delete 'events_people/:id' => 'events#leave'
  post 'events/new' => 'events#create'

  put 'forgot_password' => 'sessions#send_password_reset_instructions'

  get 'password_reset' => 'sessions#password_reset'
  put 'password_reset' => 'sessions#new_password'

  post 'persons/:id' => 'persons#parse_update'
  get 'persons/:id' => 'persons#show'

  match '/404', to: 'static_pages#404', via: :all
  match '/422', to: 'static_pages#404', via: :all
  match '/500', to: 'static_pages#404', via: :all

  resources :events
  resources :persons

  # Include the authenticity token in remote forms.
  #config.action_view.embed_authenticity_token_in_remote_forms = true


  root 'static_pages#home'
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
