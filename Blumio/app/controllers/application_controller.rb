class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  before_filter :require_login
  PublicActivity::StoreController

  private

  def require_login
    unless current_person # or !session.nil? -> to be studied/tested
      redirect_to login_path
    end
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end
end
