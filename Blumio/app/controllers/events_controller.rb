class EventsController < ApplicationController

  def searchListAll

    @search = Sunspot.search(Event) do

      with(:date).greater_than(Time.now)
    end

    @events = @search.results
    @sports = Sports.all


  end

  def searchList
    @sports = Sports.all
    #  @e = Event.all
    #   @e.each do |ev|
    #     # puts "ID EVENTO DESPORTO: " + ev.idDesporto
    #
    #   end

    #puts "MEU IP: " + request.remote_ip



    @search = Sunspot.search(Event) do



      if params[:criador].eql?("true")
        with :creator, current_person.id
      end

      with :sports_id, params[:desportoId] if params[:desportoId].present?

      with(:numParticipantes).greater_than_or_equal_to(params[:numMinParticipantes]) if params[:numMinParticipantes].present?
      with(:numParticipantes).less_than_or_equal_to(params[:numMaxParticipantes]) if params[:numMaxParticipantes].present?



      if(params[:dataFilter].present?)
        if(params[:dataFilter].eql?("data"))
          with(:date).less_than_or_equal_to(params[:timeUpperLimit]) if params[:timeUpperLimit].present?
          with(:date).greater_than_or_equal_to(params[:timeLowerLimit]) if params[:timeLowerLimit].present?
        end
        if (params[:dataFilter].eql?("hoje"))
          with(:date).less_than(Time.now + 1.day)
          with(:date).greater_than(Time.now)
        end
        if (params[:dataFilter].eql?("amanha"))
          with(:date).less_than(Time.now + 2.days)
          with(:date).greater_than(Time.now + 1.day)
        end
        if (params[:dataFilter].eql?("proxSemana"))
          with(:date).less_than(Time.now + 7.days)
          with(:date).greater_than(Time.now)
        end
      elsif
        with(:date).greater_than(Time.now)
        end

      if(params[:localizacao].present?)
        if(params[:localizacao].eql?("sim") and params[:lat].present? and params[:lon].present? and params[:raioLocal].present?)
          with(:location).in_radius(params[:lat], params[:lon], params[:raioLocal])
        end
      end




    end

    @events = @search.results


  end


  def searchAll
    @search = Sunspot.search(Event) do

      with(:date).greater_than(Time.now)
    end

    @events = @search.results
    @sports = Sports.all

  end

  def search
    @sports = Sports.all
    @e = Event.all
    @e.each do |ev|
      puts "Evento Num Pessoas: " + ev.numParticipantes.to_s
    end

    #puts "MEU IP: " + request.remote_ip


    @search = Sunspot.search(Event) do
      if(params[:dataFilter].present?)
        if(params[:dataFilter].eql?("data"))
          with(:date).less_than_or_equal_to(params[:timeUpperLimit]) if params[:timeUpperLimit].present?
          with(:date).greater_than_or_equal_to(params[:timeLowerLimit]) if params[:timeLowerLimit].present?
        end
        if (params[:dataFilter].eql?("hoje"))
          with(:date).less_than(Time.now + 1.day)
          with(:date).greater_than(Time.now)
        end
        if (params[:dataFilter].eql?("amanha"))
          with(:date).less_than(Time.now + 2.days)
          with(:date).greater_than(Time.now + 1.day)
        end
        if (params[:dataFilter].eql?("proxSemana"))
          with(:date).less_than(Time.now + 7.days)
          with(:date).greater_than(Time.now)
        end
      elsif
      with(:date).greater_than(Time.now)
      end

      if(params[:localizacao].present?)
        if(params[:localizacao].eql?("sim") and params[:lat].present? and params[:lon].present? and params[:raioLocal].present?)
          with(:location).in_radius(params[:lat], params[:lon], params[:raioLocal])
        end
      end

      with :sports_id, params[:desportoId] if params[:desportoId].present?

      with(:numParticipantes).greater_than_or_equal_to(params[:numMinParticipantes]) if params[:numMinParticipantes].present?
      with(:numParticipantes).less_than_or_equal_to(params[:numMaxParticipantes]) if params[:numMaxParticipantes].present?



    end

    @events = @search.results
  end

	def new
    @sports = Sports.all
	end
	
	def show
	  @event = Event.find(params[:id])
	  @persons = @event.persons
    @creator = Person.find(@event.creator)
	end
	
	
	def create
    @sport = Sports.find_by_sport(params[:radioDesporto])
    @position = params[:coordenadas]
    @position = @position.gsub(/\s+/, "")
    @position = @position.gsub(/[()]/, "")
    @limit = nil

    if (params[:participantes] != "")
      @limit = params[:participantes]
    end

    @event = Event.new(name: params[:nomeEvento], position: @position, date: (params[:data] + " " + params[:hora] + ":00"),
                       description: params[:descricaoEvento], creator: current_person.id, sports_id: @sport.id,
                        city: params[:morada], limitOfParticipants: @limit)


    @event.save

    @event_people = EventsPeople.new(event_id: @event.id, person_id: current_person.id)
    @event_people.save

		redirect_to @event
  end

  def join_aux
    @event_people = EventsPeople.new(event_id: params[:id], person_id: current_person.id)
    @event_people.save
    @event_people.activity_params = {:event_id => params[:id]}
    @event_people.create_activity action: :join, recipient: Person.find(@event.creator), owner: current_person
  end

  def join
    @event = Event.find(params[:id])
    if (@event.limitOfParticipants.nil?)
      join_aux
    elsif (@event.numParticipantes < @event.limitOfParticipants)
      join_aux
    end

    respond_to do |format|
      format.js
      format.html { redirect_to @event }
    end
  end

  def leave
    @event_people = EventsPeople.where(event_id:params[:id]).where(person_id: current_person.id)
    @event_people.delete_all
    @event = Event.find(params[:id])

    respond_to do |format|
      format.js
      format.html { redirect_to @event }
    end
  end

  def destroy
    @event = Event.find(params[:id])
    @creator = Person.find(@event.creator)

    # Manda notificacao a avisar que o evento foi apagado com sucesso
    @event.activity_params = {:event_name => @event.name}
    @event.create_activity action: :destroy_creator, recipient: current_person, owner: current_person

    # Manda notificacoes a avisar as pessoas que iam ao evento, que o evento foi cancelado
    quemvai = EventsPeople.where(event_id: params[:id])

    for linha in quemvai do
      @event.activity_params = {:event_name => @event.name, :event_date => @event.date}
      @event.create_activity action: :destroy, recipient: Person.find(linha.person_id), owner: @creator
    end

    # Apaga notificacoes referentes a este evento
    chave = ":event_id: '#{params[:id]}'"
    paraApagar = PublicActivity::Activity.where("parameters like :name1",{:name1 => "%#{chave}%"})

    for linha in paraApagar do
      linha.destroy
    end

    @event.destroy
    redirect_to home_url
  end

  private
  def event_params
    params.require(:event).permit(:name, :sport, :position, :date, :description)
  end
end
