class SessionsController < ApplicationController

  skip_before_filter :require_login
  def new
    if logged_in?
      redirect_to root_url
    else
      render 'new'
    end
  end

  def create
    @person = Person.find_by(email: params[:session][:email].downcase)
    if @person && @person.authenticate(params[:session][:password_digest])
      # Log the user in and redirect to the user's show page.
      #Defined in app/controllers/helpers
      log_in @person
      redirect_to root_url
    else
      # Create an error message.
      flash[:danger] = 'Invalid email/password combination'
      redirect_to login_url
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end

  def password_reset

    token = params.first[0]
    @person = Person.find_by_password_reset_token(token)

    if @person.nil?
      flash[:danger] = 'You have not requested a password reset.'
      redirect_to login_url
      return
    end

    if @person.password_expires_after < DateTime.now
      clear_password_reset(@person)
      @person.save
      flash[:info] = 'Password reset has expired. Please request a new password reset.'
      redirect_to forgot_password_url
    end
  end

  def send_password_reset_instructions
    email = params[:person][:email]

    person = Person.find_by_email(email)

    if person
      person.password_reset_token = SecureRandom.urlsafe_base64
      person.password_expires_after = 24.hours.from_now
      person.save
      UserMailer.reset_password_email(person).deliver
      flash[:warning] = 'Password instructions have been mailed to you. Please check your inbox.'
      redirect_to login_url
    else
      flash[:danger] = 'Invalid email'
      redirect_to login_url
    end
  end

  def new_password

    @token = params[:token]
    @person = Person.find_by_password_reset_token(@token)

    if verify_new_password(params[:person])
      @person.password = params[:person][:password]

      if @person.valid?
        clear_password_reset(@person)
        @person.save
        flash[:success] = 'Your password has been reset. Please sign in with your new password.'
        redirect_to login_url
      else
        redirect_to :back
      end
    else
      flash[:danger] = 'Password cannot be blank and must match the password verification.'
      redirect_to :back
    end

  end

  private
  def clear_password_reset(person)
    person.password_expires_after = nil
    person.password_reset_token = nil
  end

  def verify_new_password(passwords)
    result = true

    if passwords[:password].blank? || (passwords[:password] != passwords[:password_confirmation])
      result = false
    end

    result
  end
end