class PersonsController < ApplicationController
  skip_before_filter :require_login, :only => [:create]

  def create
    @person = Person.new(person_params)

    if @person.save
      UserMailer.welcome_email(@person).deliver

      log_in @person
      redirect_to @person
    else
      flash[:danger] = ''
      @person.errors.each do |key, error_message|
        flash[:danger] << error_message+".\n"
      end
      redirect_to login_url
    end
  end
  
  def show
    @person = Person.find(params[:id])
  end
  
  def parse_update
    Person.update(current_person.id, person_params)
    redirect_to current_person
  end

  private
  def person_params
    params.require(:person).permit(:birthday, :first_name, :last_name, :password, :password_confirmation, :email, :image)
  end

end
