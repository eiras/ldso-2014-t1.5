module SessionsHelper
# Logs in the given user.
  def log_in(person)
    session[:person_id] = person.id
  end

  # Returns the current logged-in user (if any).
  def current_person
    @current_person ||= Person.find_by(id: session[:person_id])
  end

  def does_current_person_go_to_event? (event_id)
    EventsPeople.where(event_id: event_id).where(person_id: current_person.id).any?
  end

  def logged_in?
    !current_person.nil?
  end

  def log_out
    session.delete(:person_id)
    session.clear

    reset_session
    @current_person=session[:person_id]=nil
  end
end
