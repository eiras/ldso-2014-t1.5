class Person < ActiveRecord::Base
  has_and_belongs_to_many :events
  has_secure_password
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, on: :create, message: 'Email format not correct' }
  validates_uniqueness_of :email, message: 'Email already exists in registed users'
  validates :birthday, :first_name, :last_name, :password_digest, presence: { message: 'Please fill all the required fields' }
  has_attached_file :image, styles: { med: "100x100", large: "240x240" },
                    #default_url: lambda { |image| ActionController::Base.helpers.asset_path('user.png') }
            default_url: ActionController::Base.helpers.asset_path('user.png')
  validates_attachment :image,
                       :content_type => { :content_type => ["image/jpeg", "image/gif", "image/png"] },
                       :size => { :in => 0..1000.kilobytes }


  def full_name
    first_name + " " + last_name
  end

  def activities
    PublicActivity::Activity.order(created_at: :desc).where(recipient_id: id)
  end

  def to_s
    "#{first_name} #{last_name}"
  end
  
end
