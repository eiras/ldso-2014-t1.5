class EventsPeople < ActiveRecord::Base
  validates_uniqueness_of :event_id, scope: :person_id
  include PublicActivity::Model
  tracked only: []
end
