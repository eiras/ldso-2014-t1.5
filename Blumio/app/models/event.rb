class Event < ActiveRecord::Base
  has_and_belongs_to_many :persons, :dependent => :delete_all
  belongs_to :sports
  include PublicActivity::Model
  tracked only: []


  def to_s
    "#{name}"
  end

  def lat
    self.position.split(",", 2)[0]
  end

  def lon
    self.position.split(",", 2)[1]
  end

def numParticipantes
  self.persons.count
end

  def criadorName
    Person.find(creator).full_name
  end

  searchable do
    time :date
    integer :sports_id
    latlon(:location) { Sunspot::Util::Coordinates.new(lat, lon) }
    integer :numParticipantes
    integer :creator
  end
end
