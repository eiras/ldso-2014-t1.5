ActiveAdmin.register Event do

  permit_params :name, :sports_id, :position, :date, :description, :creator

  index do
    column :id do |event|
      link_to event.id, admin_event_path(event)
    end

    column :name do |event|
      link_to event.name, admin_event_path(event)
    end

    column :sports_id do |sports|
      Sports.find(sports.sports_id)
    end
    column :date
    column :created_at
    column :creator do |event|
      link_to Person.find(event.creator), admin_person_path(Person.find(event.creator))
    end
    actions
  end

  form do |f|
    f.inputs "Events Details" do
      f.input :name
      f.input :sports_id, :as => :select, :collection => Sports.all
      f.input :position
      f.input :date
      f.input :description
      f.input :creator, :as => :select, :collection => Person.all
    end
    f.actions
  end

  sidebar 'People Attending', :only => :show do
    ul do
      EventsPeople.where(event_id: resource).collect do |eventspeople|
        li link_to Person.find_by(id: eventspeople.person_id), admin_person_path(Person.find_by(id: eventspeople.person_id))
      end
    end
  end

  show :title => :name do
    panel "Event Details" do
      attributes_table_for(event) do
        row("ID") { event.id }
        row("Name") { event.name }
        row("Sport") {event.sports_id }
        row("Position") { event.position }
        row("Date") { event.date}
        row("Description") { event.description }
        row("Created At") { event.created_at }
        row("Updated At") { event.updated_at }
        row("Creator"){ link_to Person.find_by(id: event.creator), admin_person_path(Person.find_by(id: event.creator)) }
      end
    end


  end


  filter :creator, :as => :select, :collection => Person.all
  filter :sports_id, :as => :select, :collection => Sports.all
  filter :id
  filter :name
  filter :date

end
