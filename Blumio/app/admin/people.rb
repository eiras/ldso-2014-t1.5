ActiveAdmin.register Person do

  index do
    column :id
    column :email
    column :first_name
    column :last_name
    column :created_at
    column :photo_path
    column :birthday

    actions
  end

  sidebar 'Events\' Attending', :only => :show do
    ul do
      EventsPeople.where(person_id: resource).collect do |eventspeople|
        li link_to Event.find_by(id: eventspeople.event_id), admin_event_path(Event.find_by(id: eventspeople.event_id))
      end
    end
  end

  sidebar 'Events Created', :only => :show do
    ul do
      Event.where(creator: resource).collect do |eventspeople|
        li link_to Event.find_by(id: eventspeople.id), admin_event_path(Event.find_by(id: eventspeople.id))
      end
    end
  end


  filter :id
  filter :created_at
  filter :email
  filter :last_name
  filter :first_name
  filter :birthday
  filter :events, :label => 'Eventos que vai'
end
