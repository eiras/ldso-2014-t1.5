require 'digest/sha2'
class UserMailer < ActionMailer::Base
  default from: 'blumiosmf@gmail.com'
  default "Message-ID"=>"<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@gmail.com>"
  #default "Message-ID" => lambda {"<#{SecureRandom.uuid}@gmail.com>"}

  def welcome_email(person)
    @person = person
    @url  = 'ldso05.fe.up.pt'+login_path
    mail(to: @person.email, subject: 'Welcome to Blumio')
  end

  def reset_password_email(person)

    @person = person
    @password_reset_url = 'ldso05.fe.up.pt' + password_reset_path + '?' + @person.password_reset_token
    mail(to: @person.email, subject: 'Blumio Password Reset Instructions.')

  end
end
