# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
mapLoaded = false
marker = null

@atualizaPag6 = ->
  adjust()
  data = $("[name=\"data\"]").val()
  data = "Sem data definida"  if data is ""
  hora = $("[name=\"hora\"]").val()
  hora = "Sem hora definida"  if hora is ""
  nome = $("[name=\"nomeEvento\"]").val()
  nome = "Sem nome definido"  if nome is ""
  descricao = $("[name=\"descricaoEvento\"]").val()
  descricao = "Sem descricao"  if descricao is ""
  desporto = $("[name=\"radioDesporto\"]:checked").val()
  desportoClass = $("[name=\"radioDesporto\"]:checked + a").attr("class")
  limiteParticipantes = $("[name=\"limiteParticipantes\"]:checked").val()
  participantes = $("[name=\"participantes\"]").val()
  morada = $("[name=\"morada\"]").val()
  morada = "sem morada"  if morada is ""
  console.log "data : ", data, "   hora: ", hora, "   nome: ", nome, "   desporto: ", desporto, "   desportoClass: ", desportoClass, "   descricao: ", descricao, "   morada: ", morada
  $("#desporto").html desporto
  $("#dataHora").html data + " " + hora
  if limiteParticipantes is "sim"
    $("#limite").html "Limite de <b>" + participantes + "</b> pessoas"
  else if limiteParticipantes is "nao"
    $("#limite").html "Sem limite de participantes"
  else
    $("#limite").html "não definido limite de participantes"
  $("#evento_nome").html nome
  $("#evento_descricao").html descricao
  $("#desporto_icone").attr "class", desportoClass
  $("#local_name").html morada
  unless marker

  else
    initializeMapWithLocation()
@getActive = ->
  pag = 0
  i = 1

  while i < 7
    p = "#pag" + i
    if $(p).hasClass("pagAtiva")
      pag = i
      break
    i++
  pag
@nextPage = ->
  pageId = getActive()
  console.log pageId
  if pageId < 6
    $("#pag" + pageId).fadeOut 500, ->
      $("#pag" + pageId).toggleClass "pagAtiva"
      pageId++
      $("#pag" + pageId).show()
      $("#pag" + pageId).toggleClass "pagAtiva"
      if pageId is 4 and not mapLoaded
        getLocation()
        mapLoaded = true
      atualizaPag6()  if pageId is 6

  else $("#formEvent").submit()  if pageId is 6
@previousPage = ->
  pageId = getActive()
  console.log pageId
  if pageId > 1
    $("#pag" + pageId).fadeOut 500, ->
      $("#pag" + pageId).toggleClass "pagAtiva"
      pageId--
      $("#pag" + pageId).show()
      $("#pag" + pageId).toggleClass "pagAtiva"

@showNumParticipantes = ->
  $("#numParticipantes").fadeIn 500
@hideNumParticipantes = ->
  $("#numParticipantes").fadeOut 500
@geocodePosition = (pos) ->
  geocoder.geocode
    latLng: pos
  , (responses) ->
    if responses and responses.length > 0
      updateMarkerAddress responses[0].formatted_address
    else
      updateMarkerAddress "Endereco nao disponivel."

@updateMarkerAddress = (str) ->
  document.getElementById("address").innerHTML = str
  $("[name=\"morada\"]").val str
@getLocation = ->
  if navigator.geolocation
    navigator.geolocation.getCurrentPosition initialize, initializeDefault
  else
@initialize = (position) ->
  lat = position.coords.latitude
  lon = position.coords.longitude
  startMap lat, lon
@initializeDefault = (error) ->
  switch error.code
    when error.PERMISSION_DENIED
      alert "User denied the request for Geolocation. Map will Start in Porto city"
    when error.POSITION_UNAVAILABLE
      alert "Location information is unavailable. Map will Start in Porto city"
    when error.TIMEOUT
      alert "The request to get user location timed out. Map will Start in Porto city"
    when error.UNKNOWN_ERROR
      alert "An unknown error occurred. Map will Start in Porto city"
  startMap 41.15, -8.6166667
@startMap = (lat, lon) ->
  latLng = new google.maps.LatLng(lat, lon)
  map = new google.maps.Map(document.getElementById("mapCanvas"),
    zoom: 8
    center: latLng
    mapTypeId: google.maps.MapTypeId.ROADMAP
  )
  marker = new google.maps.Marker(
    position: latLng
    title: "Local Evento"
    map: map
    draggable: true
  )

  # Update current position info.
  #updateMarkerPosition(latLng);
  geocodePosition latLng

  # Add dragging event listeners.
  google.maps.event.addListener marker, "dragstart", ->
    updateMarkerAddress "Dragging..."

  google.maps.event.addListener marker, "drag", ->


    #updateMarkerStatus('Dragging...');
    #updateMarkerPosition(marker.getPosition());
  google.maps.event.addListener marker, "dragend", ->

    #updateMarkerStatus('Drag ended');
    geocodePosition marker.getPosition()

@initializeMapWithLocation = ->
  pos = marker.position
  mapOptions =
    zoom: 6
    center: pos

  map = new google.maps.Map(document.getElementById("map_canvas1"), mapOptions)
  marker1 = new google.maps.Marker(
    position: pos
    map: map
  )
@ValidationEvent = ->
  data = $("[name=\"data\"]").val()
  hora = $("[name=\"hora\"]").val()
  nome = $("[name=\"nomeEvento\"]").val()
  descricao = $("[name=\"descricaoEvento\"]").val()
  desporto = $("[name=\"radioDesporto\"]:checked").val()
  desportoClass = $("[name=\"radioDesporto\"]:checked + a").attr("class")
  limiteParticipantes = $("[name=\"limiteParticipantes\"]:checked").val()
  participantes = $("[name=\"participantes\"]").val()
  morada = $("[name=\"morada\"]").val()
  if data is "" or hora is "" or nome is "" or descricao is "" or desporto is "" or morada is "" or limiteParticipantes is ""
    alert "Faltam Preencher campos"
    false
  else if limiteParticipantes is "sim" and participantes is ""
    alert "Falta Preencher o limite de participantes"
    false
  else
    coordenadas = marker.getPosition().toString()
    $("#formEvent").append "<input type='hidden' name='coordenadas' value='" + coordenadas + "' />"
    true

geocoder = new google.maps.Geocoder()
@adjust = ->
  fontSize = parseInt($(".leftArrow-div").width())
  if fontSize > 45
    fontSize = 45
  else fontSize = 25  if fontSize < 25
  fontSize = fontSize + "px"
  $(".leftArrow-div i").css "font-size", fontSize
  fontSize = parseInt($(".rightArrow-div").width())
  if fontSize > 45
    fontSize = 45
  else fontSize = 25  if fontSize < 25
  fontSize = fontSize + "px"
  $(".rightArrow-div i").css "font-size", fontSize
  fontSize = parseInt($(".icone-div").width() - 5)
  $(".icone-div a").css "font-size", fontSize
  fontSize = parseInt($(".icone-div1").width() - 5)
  $(".icone-div1 a").css "font-size", fontSize

$(document).ready adjust
$(window).resize adjust

@showDataForm = ->
  $("#data").css "width", 250
  return
@hideData = ->
  $("#data").css "width", 0
  return
@showDataSelection = ->
  $("#dataSelection").fadeIn 500
  return
@hideDataSelection = ->
  $("#dataSelection").fadeOut 500
  return
@showDesportoForm = ->
  $("#desporto").css "width", 250
  return
@hideDesporto = ->
  $("#desporto").css "width", 0
  return
@showAdesaoForm = ->
  $("#adesao").css "width", 250
  return
@hideAdesao = ->
  $("#adesao").css "width", 0
  return
@showPertoDeMimForm = ->
  $("#pertoDeMim").css "width", 250
  return
@hidePertoDeMim = ->
  $("#pertoDeMim").css "width", 0
  return
@showLocationSelection = ->
  $("#locationSelection").fadeIn 500
  return
@hideLocationSelection = ->
  $("#locationSelection").fadeOut 500
  return

#-------------------------------------------------------------------------------------

@getLocationPesquisa = ->
  if navigator.geolocation
    navigator.geolocation.getCurrentPosition showPosition, showError
  else
    alert("Geolocation is not supported by this browser.")
  return
@showPosition = (position) ->
  lat = position.coords.latitude
  lon = position.coords.longitude
  latlon = new google.maps.LatLng(lat, lon)
  mapholder = document.getElementById("mapholder")
  mapholder.style.height = "350px"
  mapholder.style.width = "230px"
  elemLat = document.getElementById("lat")
  elemLat.value = lat
  elemLon = document.getElementById("lon")
  elemLon.value = lon
  myOptions =
    center: latlon
    zoom: 14
    mapTypeId: google.maps.MapTypeId.ROADMAP
    mapTypeControl: false
    navigationControlOptions:
      style: google.maps.NavigationControlStyle.SMALL

  map = new google.maps.Map(document.getElementById("mapholder"), myOptions)
  marker = new google.maps.Marker(
    position: latlon
    map: map
    title: "You are here!"
  )
  return
@showError = (error) ->
  switch error.code
    when error.PERMISSION_DENIED
      alert("User denied the request for Geolocation.");
    when error.POSITION_UNAVAILABLE
     alert("Location information is unavailable.")
    when error.TIMEOUT
      alert("The request to get user location timed out.")
    when error.UNKNOWN_ERROR
      alert("An unknown error occurred.")
