class AddIdToEventsPeople < ActiveRecord::Migration
  def change
    add_column :events_people, :id, :primary_key
  end
end
