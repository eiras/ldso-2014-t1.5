class AddNotNullToPeopleFields < ActiveRecord::Migration
  def change
    change_column :people, :email, :string, :null => false
    change_column :people, :last_name, :string, :null => false
    change_column :people, :first_name, :string, :null => false
    change_column :people, :password_digest, :string, :null => false
  end
end
