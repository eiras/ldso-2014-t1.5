class AddPasswordResetFields < ActiveRecord::Migration
  def change
    add_column :people, :password_reset_token, :string
    add_column :people, :password_expires_after, :datetime
  end
end
