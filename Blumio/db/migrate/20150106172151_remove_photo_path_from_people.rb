class RemovePhotoPathFromPeople < ActiveRecord::Migration
  def change
    remove_column :people, :photo_path, :string
  end
end
