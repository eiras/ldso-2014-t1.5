class MudarNomesCiolunasEventsPeople < ActiveRecord::Migration
  def change
    rename_column :events_people, :people_id, :person_id
    rename_column :events_people, :events_id, :event_id
  end
end
