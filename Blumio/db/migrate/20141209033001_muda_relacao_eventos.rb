class MudaRelacaoEventos < ActiveRecord::Migration
  def change
    rename_column :events, :person_id, :creator
  end
end
