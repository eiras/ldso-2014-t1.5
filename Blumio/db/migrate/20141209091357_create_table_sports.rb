class CreateTableSports < ActiveRecord::Migration
  def change
    create_table :sports do |t|
      t.string :icon
      t.string :sport
      t.timestamps
    end
  end
end
