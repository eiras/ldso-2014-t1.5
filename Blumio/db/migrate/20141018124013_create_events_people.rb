class CreateEventsPeople < ActiveRecord::Migration
  def change
    create_table :events_people,:id => false do |t| 
        t.references :event
        t.references :person
        t.timestamps
    end
    
  end
end
