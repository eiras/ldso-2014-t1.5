class AddSportsToEvents < ActiveRecord::Migration
  def change
    add_reference(:events, :sports, index: true)
  end
end
