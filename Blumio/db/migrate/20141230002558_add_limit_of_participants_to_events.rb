class AddLimitOfParticipantsToEvents < ActiveRecord::Migration
  def change
    add_column :events, :limitOfParticipants, :integer
  end
end
