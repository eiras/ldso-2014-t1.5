class ChangeDefaultValuePathFotoPeople < ActiveRecord::Migration
  def change
    change_column_default :people, :photo_path, "/img/user.png"
  end
end
