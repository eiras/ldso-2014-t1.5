class AddLastNameToPeople < ActiveRecord::Migration
  def change
    add_column :people, :last_name, :string
    add_column :people, :first_name, :string
    remove_column :people, :name
    remove_column :people, :age
    add_column :people, :photo_path, :string
    add_column :people, :birthday, :date
  end
end
