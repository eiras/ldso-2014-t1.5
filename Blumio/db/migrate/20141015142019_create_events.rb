class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.string :sport
      t.string :position
      t.timestamp :date
      t.text :description

      t.timestamps
    end
  end
end
