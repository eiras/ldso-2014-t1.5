class DropTableActiveAdminComments < ActiveRecord::Migration
  def change
    if ActiveRecord::Base.connection.table_exists? 'active_admin_comments'
      drop_table 'active_admin_comments'
    end
  end
end
