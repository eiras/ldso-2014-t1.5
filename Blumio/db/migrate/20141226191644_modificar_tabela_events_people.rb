class ModificarTabelaEventsPeople < ActiveRecord::Migration
  def change
    remove_column :events_people, :event_id
    remove_column :events_people, :person_id
    add_reference :events_people, :events
    add_reference :events_people, :people
  end
end
