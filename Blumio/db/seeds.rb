#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

people_list = [ #cenas with digest = $2a$10$6k0JB4UzRM9dcxNkKzn0seBGt1M6aAKTvQF3jFUXWWqFSCDdvh/5S
    [ 'teste1@blumio.com', 'Ricardo', 'Salgado', '1980-05-02', '$2a$10$6k0JB4UzRM9dcxNkKzn0seBGt1M6aAKTvQF3jFUXWWqFSCDdvh/5S' ],
    [ 'ei11059@fe.up.pt', 'Luís', 'Araújo',  '1993-05-12', '$2a$10$6k0JB4UzRM9dcxNkKzn0seBGt1M6aAKTvQF3jFUXWWqFSCDdvh/5S' ],
    [ 'ei11091@fe.up.pt', 'João', 'Pinto', '1993-01-02', '$2a$10$6k0JB4UzRM9dcxNkKzn0seBGt1M6aAKTvQF3jFUXWWqFSCDdvh/5S' ]
]

events_list = [
    [ 'Corrida do MIEIC', '41.17790656432498,-8.59870629882812', '2015-01-06 14:00', 'Nesta altura de exames, vamos correr para descomprimir um pouco do stress', 2, 5, 'Rua Dom Frei Vicente da Soledade e Castro, 4200-465 Porto, Portugal'],
    [ 'Futebolada do MIEIC', '41.17688907357984,-8.593728118896479', '2015-01-12 18:00:00', 'A futebolada do costume. Ao fim vamos às francesinhas!', 2, 1, 'Travessa de Lamas, 4200 Porto, Portugal' ],
    [ 'Circuito de Manutenção', '41.1535308938182,-8.642673069000239', '2015-01-10 16:00:00', 'Vamos jogar ping-pong nas mesas do parque!', 1, 10, 'Faculdade de Letras de Universidade do Porto, Rua do Campo Alegre 1191, 4150-173 Porto, Portugal',2 ],
    [ 'Caminhada pelo Porto', '41.14575106291287,-8.615046316146845', '2015-01-01 15:00:00', 'O objectivo é fazer uma caminhada de cerca de 3 horas pelos pontos emblemáticos da cidade...', 3, 6, 'Rua de São Filipe de Nery, Porto, Portugal' ]
]

sports_list = [
    ['sports-football118','Futebol' ],
    ['sports-tennis18','Tenis' ],
    ['sports-golf22','Golf' ],
    ['sports-basketball35','Basket' ],
    ['sports-running31','Corrida' ],
    ['sports-walking17','Caminhada' ],
    ['sports-man459','Ciclismo' ],
    ['sports-volleyball3','Voleibol' ],
    ['sports-swimming22','Natação' ],
    ['sports-ping3','Ping-Pong' ],
    ['sports-skateboard3','Skateboarding' ],
    ['sports-canoeing1','Canoagem' ]
]

events_people_list = [
    [ 1, 1 ],
    [ 1, 2 ],
    [ 2, 2 ],
    [ 2, 3 ],
    [ 3, 1 ]
]

sports_list.each do |icon, sport|
  Sports.create( icon: icon, sport: sport )
end

people_list.each do |email, first_name, last_name, birthday, password_digest|
  Person.create( email: email, first_name: first_name, last_name: last_name, birthday: birthday, password_digest: password_digest )
end

events_list.each do |name, position, date, description, creator, sports_id, city, limitOfParticipants|
  Event.create( name: name, position: position, date: date, description: description, creator: creator, sports_id: sports_id, city: city, limitOfParticipants: limitOfParticipants )
end


events_people_list.each do |event_id, person_id|
  EventsPeople.create( event_id: event_id, person_id: person_id )
end


AdminUser.create(email: 'ei11143@fe.up.pt', password: 'password', password_confirmation: 'password')