# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150106172626) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["owner_id", "owner_type"], :name => "index_activities_on_owner_id_and_owner_type"
    t.index ["recipient_id", "recipient_type"], :name => "index_activities_on_recipient_id_and_recipient_type"
    t.index ["trackable_id", "trackable_type"], :name => "index_activities_on_trackable_id_and_trackable_type"
  end

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["email"], :name => "index_admin_users_on_email", :unique => true
    t.index ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true
  end

  create_table "admins", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "people", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest",        null: false
    t.string   "email",                  null: false
    t.string   "last_name",              null: false
    t.string   "first_name",             null: false
    t.date     "birthday"
    t.string   "password_reset_token"
    t.datetime "password_expires_after"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "sports", force: true do |t|
    t.string   "icon"
    t.string   "sport"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "events", force: true do |t|
    t.string   "name"
    t.string   "position"
    t.datetime "date"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "creator"
    t.integer  "sports_id"
    t.string   "city"
    t.integer  "limitOfParticipants"
    t.index ["creator"], :name => "index_events_on_creator"
    t.index ["sports_id"], :name => "index_events_on_sports_id"
    t.foreign_key ["creator"], "people", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_events_person_id"
    t.foreign_key ["sports_id"], "sports", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_events_sports_id"
  end

  create_table "events_people", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "event_id"
    t.integer  "person_id"
    t.index ["event_id"], :name => "fk__events_people_events_id"
    t.index ["person_id"], :name => "fk__events_people_people_id"
    t.foreign_key ["event_id"], "events", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_events_people_events_id"
    t.foreign_key ["person_id"], "people", ["id"], :on_update => :no_action, :on_delete => :no_action, :name => "fk_events_people_people_id"
  end

  create_table "users", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
