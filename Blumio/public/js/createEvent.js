var mapLoaded = false;
var marker;
var geocoder = new google.maps.Geocoder();

var adjust = function()
{
	var fontSize = parseInt($(".leftArrow-div").width());
	if(fontSize>45)
		fontSize = 45;
	else if(fontSize<25)
		fontSize=25;

	fontSize = fontSize + "px";
//alert(fontSize);
	$(".leftArrow-div i").css('font-size', fontSize);

	var fontSize = parseInt($(".rightArrow-div").width());
	if(fontSize>45)
		fontSize = 45;
	else if(fontSize<25)
		fontSize=25;

	fontSize = fontSize + "px";
	//alert(fontSize);
	$(".rightArrow-div i").css('font-size', fontSize);

	var fontSize = parseInt($(".icone-div").width()-5 );

	$(".icone-div a").css('font-size', fontSize);

	var fontSize = parseInt($(".icone-div1").width()-5 );

	$(".icone-div1 a").css('font-size', fontSize);

};



function atualizaPag6()
{
	adjust();

	var data = $('[name="data"]').val();
	if (data == "")
	{
		data ="Sem data definida";
	}

	var hora = $('[name="hora"]').val();
	if(hora =="")
		hora ="Sem hora definida";


	var nome = $('[name="nomeEvento"]').val();
	if(nome =="")
		nome = "Sem nome definido";

	var descricao = $('[name="descricaoEvento"]').val();
	if(descricao=="")
		descricao ="Sem descricao";

	var desporto = $('[name="radioDesporto"]:checked').val();

	var desportoClass = $('[name="radioDesporto"]:checked + a').attr('class');
	var limiteParticipantes = $('[name="limiteParticipantes"]:checked').val();
	var participantes = $('[name="participantes"]').val();

	var morada = $('[name="morada"]').val();
	if(morada=="")
		morada ="sem morada";
	//var coordenadas = markers[0].getPosition().toString();

	console.log("data : ",data,"   hora: ",hora,"   nome: ",nome,"   desporto: ",desporto,"   desportoClass: ",desportoClass,"   descricao: ",descricao,"   morada: ",morada);


	$('#desporto').html(desporto);
	$('#dataHora').html(data + " " + hora);
	if(limiteParticipantes === "sim")
	{
		$('#limite').html("Limite de <b>" + participantes + "</b> pessoas");
	}
	else if(limiteParticipantes === "nao")
	{
		$('#limite').html("Sem limite de participantes");
	}
	else
	{
		$('#limite').html("não definido limite de participantes");
	}

	$('#evento_nome').html(nome);
	$('#evento_descricao').html(descricao);
	$('#desporto_icone').attr('class',desportoClass);
	$('#local_name').html(morada);

	if(!marker)
	{}
	else initializeMapWithLocation();


}


function getActive()
{
	var pag = 0;
	for (var i = 1; i < 7; i++) {
		var p = '#pag' + i;
		if($(p).hasClass('pagAtiva'))
		{
			pag = i;
			break;
		}
	};
	return pag;
};

function nextPage()
{
	var pageId = getActive();
	console.log(pageId);
	if(pageId<6)
	{
		$('#pag' + pageId).fadeOut(500, function()
		{
			$('#pag' + pageId).toggleClass('pagAtiva');
			pageId++;
			$('#pag' + pageId).show();
			$('#pag' + pageId).toggleClass('pagAtiva');

			if(pageId == 4 && !mapLoaded)
			{
				getLocation();
				mapLoaded = true;
			}

			if(pageId == 6)
			{
				atualizaPag6();
			}
		});
	}
	else if(pageId==6)
	{

		$("#formEvent").submit();

	}

};
function previousPage()
{
	var pageId = getActive();
	console.log(pageId);
	if(pageId>1)
	{
		$('#pag' + pageId).fadeOut(500, function()
		{
			$('#pag' + pageId).toggleClass('pagAtiva');
			pageId--;
			$('#pag' + pageId).show();
			$('#pag' + pageId).toggleClass('pagAtiva');
		});
	}
};


function showNumParticipantes()
{
	$('#numParticipantes').fadeIn(500);
};

function hideNumParticipantes()
{
	$('#numParticipantes').fadeOut(500);
};

$(document).ready(adjust);
$(window).resize(adjust);




function geocodePosition(pos) {
	geocoder.geocode({
		latLng: pos
	}, function(responses) {
		if (responses && responses.length > 0) {
			updateMarkerAddress(responses[0].formatted_address);
		} else {
			updateMarkerAddress('Endereco nao disponivel.');
		}
	});
}




function updateMarkerAddress(str) {
	document.getElementById('address').innerHTML = str;
	$('[name="morada"]').val(str);
}

function getLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(initialize, initializeDefault);
	} else {

	}
}



function initialize(position)
{
	lat = position.coords.latitude;
	lon = position.coords.longitude;
	startMap(lat,lon);
}

function initializeDefault(error)
{
	switch(error.code) {
		case error.PERMISSION_DENIED:
			alert("User denied the request for Geolocation.");
			break;
		case error.POSITION_UNAVAILABLE:
			alert("Location information is unavailable.");
			break;
		case error.TIMEOUT:
			alert("The request to get user location timed out.");
			break;
		case error.UNKNOWN_ERROR:
			alert("An unknown error occurred.");
			break;
	}

	startMap(41.15,-8.6166667);
}

function startMap(lat,lon) {

	var latLng = new google.maps.LatLng(lat, lon);
	var map = new google.maps.Map(document.getElementById('mapCanvas'), {
		zoom: 8,
		center: latLng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});
	marker = new google.maps.Marker({
		position: latLng,
		title: 'Local Evento',
		map: map,
		draggable: true
	});

	// Update current position info.
	//updateMarkerPosition(latLng);
	geocodePosition(latLng);

	// Add dragging event listeners.
	google.maps.event.addListener(marker, 'dragstart', function() {
		updateMarkerAddress('Dragging...');
	});

	google.maps.event.addListener(marker, 'drag', function() {
		//updateMarkerStatus('Dragging...');
		//updateMarkerPosition(marker.getPosition());
	});

	google.maps.event.addListener(marker, 'dragend', function() {
		//updateMarkerStatus('Drag ended');
		geocodePosition(marker.getPosition());
	});
}


function initializeMapWithLocation() {


	var pos = marker.position;


	var mapOptions = {
		zoom: 6,
		center: pos
	}
	var map = new google.maps.Map(document.getElementById("map_canvas1"), mapOptions);

	var marker1 = new google.maps.Marker({
		position: pos,
		map: map

	});

}


function ValidationEvent()
{
	var data = $('[name="data"]').val();
	var hora = $('[name="hora"]').val();
	var nome = $('[name="nomeEvento"]').val();
	var descricao = $('[name="descricaoEvento"]').val();
	var desporto = $('[name="radioDesporto"]:checked').val();
	var desportoClass = $('[name="radioDesporto"]:checked + a').attr('class');
	var limiteParticipantes = $('[name="limiteParticipantes"]:checked').val();
	var participantes = $('[name="participantes"]').val();
	var morada = $('[name="morada"]').val();


	if(data =="" || hora == "" || nome == "" || descricao == "" || desporto =="" || morada =="" || limiteParticipantes =="")
	{
		alert( "Faltam Preencher campos" );
		return false;
	}
	else if (limiteParticipantes =="sim" && participantes =="")
	{
		alert( "Falta Preencher o limite de participantes" );
		return false;
	}
	else
	{
		var coordenadas = marker.getPosition().toString();

		$('#formEvent').append("<input type='hidden' name='coordenadas' value='"+
		coordenadas+"' />");

		return true;
	}

}