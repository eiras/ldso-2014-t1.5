/**
 * Created by Miguel on 24-11-2014.
 */
// TOUCH-EVENTS SINGLE-FINGER SWIPE-SENSING JAVASCRIPT
// Courtesy of PADILICIOUS.COM and MACOSXAUTOMATION.COM

// this script can be used with one or more page elements to perform actions based on them being swiped with a single finger

var triggerElementID = null; // this variable is used to identity the triggering element
var fingerCount = 0;
var startX = 0;
var curX = 0;
var deltaX = 0;
var horzDiff = 0;
var minLength = 72; // the shortest distance the user may swipe
var swipeLength = 0;
var el;
var swipeDirection = null;

// The 4 Touch Event Handlers

// NOTE: the touchStart handler should also receive the ID of the triggering element
// make sure its ID is passed in the event call placed in the element declaration, like:
// <div id="picture-frame" ontouchstart="touchStart(event,'picture-frame');"  ontouchend="touchEnd(event);" ontouchmove="touchMove(event);" ontouchcancel="touchCancel(event);">

function touchStart(event,passedName) {
    // disable the standard ability to select the touched object
    event.preventDefault();
    el = document.getElementById("sidebar-wrapper");
    // get the total number of fingers touching the screen
    fingerCount = event.touches.length;
    // since we're looking for a swipe (single finger) and not a gesture (multiple fingers),
    // check that only one finger was used
    if ( fingerCount == 1 ) {
        // get the coordinates of the touch
        startX = event.touches[0].pageX;
        // store the triggering element ID
        triggerElementID = passedName;
    } else {
        // more than one finger touched so cancel
        touchCancel(event);
    }
}

function touchMove(event) {
    event.preventDefault();
    if ( event.touches.length == 1 ) {
        curX = event.touches[0].pageX;
        //var el = document.getElementById("sidebar-wrapper");
        var newWidth = curX - startX + el.offsetWidth;
        if (newWidth < 0)
            newWidth=0;
        if (newWidth > 250)
            newWidth=250;
        el.style.width = newWidth + 'px';

    } else {
        touchCancel(event);
    }
}

function touchEnd(event) {
    event.preventDefault();
    // check to see if more than one finger was used and that there is an ending coordinate
    if ( fingerCount == 1 ) {
        // use the Distance Formula to determine the length of the swipe
        swipeLength = Math.round(Math.sqrt(Math.pow(curX - startX,2)));
        if (swipeLength > 250)
            swipeLength=250;
        if (swipeLength < -250)
            swipeLength=-250;

        deltaX = swipeLength;

        console.log('swipeLength: '+swipeLength);

        removeWidth();
        // if the user swiped more than the minimum length, perform the appropriate action
        if ( swipeLength >= minLength ) {
            console.log('dentro if swipe length min ultrapassado');
            determineSwipeDirection();
            processingRoutine();
            touchCancel(event); // reset the variables
        } else {
            touchCancel(event);
        }
    } else {
        touchCancel(event);
    }
}

function touchCancel(event) {
    // reset the variables back to default values
    fingerCount = 0;
    startX = 0;
    curX = 0;
    deltaX = 0;
    horzDiff = 0;
    swipeLength = 0;
    triggerElementID = null;
    swipeDirection = null;
    el = document.getElementById("sidebar-wrapper");
}


function determineSwipeDirection() {
    if (curX > startX) {
        swipeDirection = 'right';
    } else  {
        swipeDirection = 'left';
    }
}


function removeWidth() {
    $sidebar = $("#sidebar-wrapper");

    /*
     keepStyle = {
     weight: $sidebar.css('weight')
     };
     $sidebar.removeAttr('style').css(keepStyle);
     */

    $sidebar.removeAttr('style');
}

function processingRoutine() {
    //var swipedElement = document.getElementById(triggerElementID);

    if (Math.abs(swipeLength) > 220) {

        if (swipeDirection == 'left') {

            console.log('try to toggle when swipe left');
            $("#wrapper").toggleClass("toggled");

        } else if (swipeDirection == 'right') {

            console.log('try to toggle when swipe right');
            $("#wrapper").toggleClass("toggled");

        }
    }
}