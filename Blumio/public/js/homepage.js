/**
 * Created by Miguel on 24-11-2014.
 */

$( document ).ready(function() {

    $painel_signup = $('#painel_signup');
    $painel_login = $('#painel_login');
    $painel_forgot_password = $('#painel_forgot_password');

    function removeAllCollapse() {
        $painel_login.removeClass("collapse");
        $painel_signup.removeClass("collapse");
        $painel_forgot_password.removeClass("collapse");

        $("#signup").removeClass("collapse");
        $("#login").removeClass("collapse");
        $("#forgot_password").removeClass("collapse");
    }

    $("#signup").click(function(e) {
        removeAllCollapse();
        $painel_login.addClass("collapse");
        $painel_forgot_password.addClass("collapse");
        $("#signup").addClass("collapse");
    });

    $("#login").click(function(e) {
        removeAllCollapse();
        $painel_signup.addClass("collapse");
        $painel_forgot_password.addClass("collapse");
        $("#login").addClass("collapse");
    });

    $("#forgot_password").click(function(e) {
        removeAllCollapse();
        $painel_signup.addClass("collapse");
        $painel_login.addClass("collapse");
        $("#forgot_password").addClass("collapse");
    });

});
