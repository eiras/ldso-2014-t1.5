function showDataForm()
{
    $("#data").css( "width", 250);
}

function hideData()
{
    $("#data").css( "width", 0);
}

function showDataSelection()
{
    $("#dataSelection").fadeIn(500);
}

function hideDataSelection()
{
    $("#dataSelection").fadeOut(500);
}

function showDesportoForm()
{
    $("#desporto").css( "width", 250);
}

function hideDesporto()
{
    $("#desporto").css( "width", 0);
}

function showAdesaoForm()
{
    $("#adesao").css( "width", 250);
}

function hideAdesao()
{
    $("#adesao").css( "width", 0);
}

function showPertoDeMimForm()
{
    $("#pertoDeMim").css( "width", 250);
}

function hidePertoDeMim()
{
    $("#pertoDeMim").css( "width", 0);
}

function showLocationSelection()
{
    $("#locationSelection").fadeIn(500);
}

function hideLocationSelection()
{
    $("#locationSelection").fadeOut(500);
}





/*-------------------------------------------------------------------------------------*/

var x = document.getElementById("demo");
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    lat = position.coords.latitude;
    lon = position.coords.longitude;
    latlon = new google.maps.LatLng(lat, lon)
    mapholder = document.getElementById('mapholder')
    mapholder.style.height = '350px';
    mapholder.style.width = '230px';

    elemLat = document.getElementById('lat');
    elemLat.value = lat;
    elemLon = document.getElementById('lon');
    elemLon.value = lon;


    var myOptions = {
        center:latlon,zoom:14,
        mapTypeId:google.maps.MapTypeId.ROADMAP,
        mapTypeControl:false,
        navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
    }

    var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);
    var marker = new google.maps.Marker({position:latlon,map:map,title:"You are here!"});
}

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            x.innerHTML = "User denied the request for Geolocation."
            break;
        case error.POSITION_UNAVAILABLE:
            x.innerHTML = "Location information is unavailable."
            break;
        case error.TIMEOUT:
            x.innerHTML = "The request to get user location timed out."
            break;
        case error.UNKNOWN_ERROR:
            x.innerHTML = "An unknown error occurred."
            break;
    }
}